import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app/common/controllers/authentication_controller.dart';
import 'app/common/controllers/main_controller.dart';
import 'app/components/widgets/ProgressIndicator.dart';
import 'app/components/widgets/theme.dart';
import 'app/modules/home/controllers/home_controller.dart';
import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    GetMaterialApp(
      theme: appThemeData,
      debugShowCheckedModeBanner: false,
      title: "NeoSTORE",
      // initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      home: Scaffold(body: ProgressingIndicator()),
    ),
  );
  Get.put(
      AuthenticationController()); //* This controller will decide the landing screen
  Get.put(HomeController());
  Get.put(MainController());
}
