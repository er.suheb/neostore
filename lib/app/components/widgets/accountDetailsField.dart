import 'package:flutter/material.dart';
import 'customText.dart';

class AccountDetailsField extends StatelessWidget {
  final String text;
  final IconData icon;
  const AccountDetailsField({
    required this.icon,
    required this.text,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 10,
      ),
      margin: EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(border: Border.all(color: Colors.white)),
      child: Row(children: [
        Icon(
          icon,
          color: Colors.white,
        ),
        SizedBox(width: 10),
        CustomText(
          text: text,
          color: Colors.white,
        ),
      ]),
    );
  }
}
