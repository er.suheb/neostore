import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/controllers/main_controller.dart';
import '../../modules/productDetailed/controllers/product_detailed_controller.dart';
import 'customButton.dart';
import 'customText.dart';
import 'starRating.dart';

class RatingDialog extends StatelessWidget {
  RatingDialog({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final ProductDetailedController controller;

  @override
  Widget build(BuildContext context) {
    final mainController = Get.find<MainController>();
    return AlertDialog(
      backgroundColor: Colors.white,
      alignment: Alignment.center,
      elevation: 10,
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomText(
              text: controller.productDetails.data!.name!,
              fontName: "Gotham",
              size: 20,
              color: Colors.black,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.network(
              controller.productDetails.data!
                  .productImages![controller.indexImage].image!,
              loadingBuilder: mainController.loadUntillFetch,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: StarRating(
                iconSize: 50,
                rating: controller.productDetails.data!.rating!.toDouble()),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomButton(
                navigation: () => controller.rateNow(),
                text: "RATE NOW",
                textColor: Colors.white,
                backgroundColor: Colors.red.shade700),
          )
        ],
      ),
    );
  }
}
