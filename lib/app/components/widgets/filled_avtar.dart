import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../modules/drawer/controllers/drawer_controller.dart';
import '../../modules/myAccount/controllers/my_account_controller.dart';

class FilledImageAvatar extends StatelessWidget {
 final String imageUrl;
  double radius;
  double borderRadius;
  FilledImageAvatar({
    Key? key,
    this.borderRadius = 42,
    this.radius = 40,
    required this.imageUrl
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Colors.white,
      radius: 42,
      child: CircleAvatar(
        backgroundColor: Colors.white,
        radius: 40,
        child: SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Image.network(
              imageUrl,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
