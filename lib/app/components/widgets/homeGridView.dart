import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../routes/app_pages.dart';

class GridContainer1 extends StatelessWidget {
  GridContainer1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.PRODUCTLIST, arguments: 1);
      },
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.red.shade500, Colors.red.shade900])),
        child: Stack(
          children: const [
            Positioned(
              bottom: 15,
              left: 15,
              child: Icon(
                Icons.table_bar,
                color: Colors.white,
                size: 70,
              ),
            ),
            Positioned(
              right: 15,
              top: 15,
              child: Text(
                "Tables",
                style: const TextStyle(
                    color: Colors.white, fontFamily: "Gotham", fontSize: 23),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class GridContainer2 extends StatelessWidget {
  GridContainer2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Get.toNamed(Routes.PRODUCTLIST, arguments: 3),
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.red.shade500, Colors.red.shade900])),
        child: Stack(
          children: const [
            Positioned(
              top: 15,
              right: 15,
              child: Icon(
                Icons.chair,
                color: Colors.white,
                size: 70,
              ),
            ),
            Positioned(
              bottom: 15,
              left: 15,
              child: Text(
                "Sofas",
                style: const TextStyle(
                    color: Colors.white, fontFamily: "Gotham", fontSize: 23),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class GridContainer3 extends StatelessWidget {
  GridContainer3({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Get.toNamed(Routes.PRODUCTLIST, arguments: 2),
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.red.shade500, Colors.red.shade900])),
        child: Stack(
          children: const [
            Positioned(
              top: 15,
              left: 15,
              child: Icon(
                Icons.chair_alt,
                color: Colors.white,
                size: 70,
              ),
            ),
            Positioned(
              right: 15,
              bottom: 15,
              child: Text(
                "Chairs",
                style: const TextStyle(
                    color: Colors.white, fontFamily: "Gotham", fontSize: 23),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class GridContainer4 extends StatelessWidget {
  GridContainer4({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Get.toNamed(Routes.PRODUCTLIST, arguments: 4),
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.red.shade500, Colors.red.shade900])),
        // color: Colors.red.shade800,
        child: Stack(
          children: const [
            Positioned(
              bottom: 15,
              left: 15,
              child: Icon(
                Icons.bed,
                color: Colors.white,
                size: 70,
              ),
            ),
            Positioned(
              right: 15,
              top: 15,
              child: Text(
                "Beds",
                style: const TextStyle(
                    color: Colors.white, fontFamily: "Gotham", fontSize: 23),
              ),
            )
          ],
        ),
      ),
    );
  }
}
