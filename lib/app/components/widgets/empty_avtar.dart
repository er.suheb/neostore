import 'package:flutter/material.dart';

class EmptyImageAvatar extends StatelessWidget {
  const EmptyImageAvatar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CircleAvatar(
          backgroundColor: Colors.grey.shade100,
          radius: 48,
          child: CircleAvatar(
            backgroundColor: Color.fromARGB(135, 99, 93, 93),
            backgroundImage: AssetImage("assets/images/avatar.jpg"),
            radius: 45.0,
          ),
        ),
        Positioned(
          right: 2,
          bottom: 2,
          child: Container(
            child: Icon(
              Icons.edit,
              size: 10,
            ),
            padding: EdgeInsets.all(3.5),
            decoration: BoxDecoration(
              border: Border.all(width: 1, color: Colors.black),
              borderRadius: BorderRadius.circular(90.0),
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
