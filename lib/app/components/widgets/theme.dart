import 'package:flutter/material.dart';

final ThemeData appThemeData = ThemeData(
  primaryColor: Colors.red[700],

  // fontFamily: 'GothamBold',
  textTheme: const TextTheme(
    headline1:
        TextStyle(fontSize: 35, color: Colors.white, fontFamily: "GothamBold"),
    headline2:
        TextStyle(fontSize: 28, color: Colors.white, fontFamily: "GothamBold"),
    headline3: TextStyle(
        fontSize: 13, color: Colors.white, fontFamily: "GothamMedium"),
    headline4:
        TextStyle(fontFamily: "Gotham", color: Colors.white, fontSize: 14),
    headline5:
        TextStyle(fontFamily: "Gotham", color: Colors.white, fontSize: 12),
  ),
  colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.white),
);
