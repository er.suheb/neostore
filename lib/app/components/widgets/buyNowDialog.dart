import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/controllers/main_controller.dart';
import '../../modules/productDetailed/controllers/product_detailed_controller.dart';
import '../../modules/productDetailed/models/product_detailed_model.dart';
import '../widgets/customButton.dart';

class BuyNowDialog extends GetView<ProductDetailedController> {
  final mainController = Get.find<MainController>();
  BuyNowDialog({
    this.onPressed,
    Key? key,
    required this.productDetails,
    required this.imageIndex,
  }) : super(key: key);

  final void Function()? onPressed;
  final ProductDetails productDetails;
  final int imageIndex;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        padding: EdgeInsets.all(20),
        width: Get.size.width,
        decoration: BoxDecoration(color: Colors.white),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                productDetails.data!.name!,
                style: Get.theme.textTheme.headline6!
                    .copyWith(color: Colors.black),
              ),
              SizedBox(height: 25),
              Image.network(
                productDetails.data!.productImages![imageIndex].image!,
                fit: BoxFit.contain,
                loadingBuilder: mainController.loadUntillFetch,
                // loadingBuilder: Get.find<GlobalController>().loadingBuilder,
              ),
              SizedBox(height: 40),
              Text(
                "Enter Qty",
                style: Get.theme.textTheme.headline6!
                    .copyWith(color: Colors.black.withOpacity(0.5)),
              ),
              const SizedBox(height: 25),
              SizedBox(
                width: 90,
                child: TextField(
                  controller: controller.quantityCtrl,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    filled: true,
                    // labelText: "Count",
                    contentPadding: const EdgeInsets.symmetric(
                      vertical: 16,
                      horizontal: 30,
                    ),
                    border: OutlineInputBorder(
                      // borderRadius: BorderRadius.circular(7.0),
                      borderSide: BorderSide(
                        color: Colors.red.shade700,
                        width: 2,
                      ),
                    ),
                    isDense: true,

                    labelStyle: const TextStyle(
                      fontFamily: "Gotham",
                      color: Colors.grey,
                      fontSize: 14,
                    ),
                    fillColor: Colors.transparent,
                    focusedBorder: OutlineInputBorder(
                      // borderRadius: BorderRadius.circular(7.0),
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      // borderRadius: BorderRadius.circular(7.0),
                      borderSide: BorderSide(
                        color: Colors.black.withOpacity(0.6),
                        width: 1.5,
                      ),
                    ),
                    errorStyle: TextStyle(color: Colors.black, fontSize: 10),
                    errorBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(255, 241, 241, 0.842),
                        width: 2,
                      ),
                    ),
                    // errorStyle: GoogleFonts.montserrat(
                    //     fontSize: 10, color: Colors.white.withOpacity(0.75)),
                  ),
                ),
              ),
              SizedBox(height: 25),
              CustomButton(
                navigation: onPressed,
                text: "Checkout",
                textColor: Colors.white,
                backgroundColor: Colors.red.shade700,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
