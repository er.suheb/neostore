import 'package:get/get.dart';

import '../modules/cart/bindings/my_cart_binding.dart';
import '../modules/cart/views/my_cart_view.dart';
import '../modules/drawer/bindings/drawer_binding.dart';
import '../modules/drawer/views/drawer_view.dart';
import '../modules/forgotPassword/bindings/forgot_password_binding.dart';
import '../modules/forgotPassword/views/forgot_password_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/myAccount/bindings/my_account_binding.dart';
import '../modules/myAccount/views/my_account_view.dart';
import '../modules/order/bindings/order_binding.dart';
import '../modules/order/views/orderDeatils_view.dart';
import '../modules/order/views/order_list_view.dart';
import '../modules/order/views/order_view.dart';
import '../modules/producList/bindings/productList_binding.dart';
import '../modules/producList/views/productList_View.dart';
import '../modules/productDetailed/bindings/product_detailed_binding.dart';
import '../modules/productDetailed/views/product_detailed_view.dart';
import '../modules/register/bindings/register_binding.dart';
import '../modules/register/views/register_view.dart';
import '../modules/storeLocator/bindings/store_locator_binding.dart';
import '../modules/storeLocator/views/store_locator_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.ORDER_DETAILS;

  static final routes = [
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.FORGOT_PASSWORD,
      page: () => const ForgotPasswordView(),
      binding: ForgotPasswordBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => const RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.DRAWER,
      page: () => DrawerView(),
      binding: DrawerBinding(),
    ),
    GetPage(
      name: _Paths.PRODUCTLIST,
      page: () => ProductListView(),
      binding: TablesBinding(),
    ),
    GetPage(
      name: _Paths.PRODUCT_DETAILED,
      page: () => ProductDetailedView(),
      binding: ProductDetailedBinding(),
    ),
    GetPage(
      name: _Paths.MY_CART,
      page: () => MyCartView(),
      binding: MyCartBinding(),
    ),
    GetPage(
      name: _Paths.ORDER,
      page: () => OrderView(),
      binding: OrderBinding(),
    ),
    GetPage(
      name: _Paths.MY_ORDER,
      page: () => OrderListView(),
      binding: OrderBinding(),
    ),
    GetPage(
      name: _Paths.ORDER_DETAILS,
      page: () => OrderDetails(),
      binding: OrderBinding(),
    ),
    GetPage(
      name: _Paths.MY_ACCOUNT,
      page: () => MyAccountView(),
      binding: MyAccountBinding(),
    ),
    GetPage(
      name: _Paths.STORE_LOCATOR,
      page: () => const StoreLocatorView(),
      binding: StoreLocatorBinding(),
    ),
  ];
}
