part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const LOGIN = _Paths.LOGIN;
  static const FORGOT_PASSWORD = _Paths.FORGOT_PASSWORD;
  static const REGISTER = _Paths.REGISTER;
  static const HOME = _Paths.HOME;
  static const DRAWER = _Paths.DRAWER;
  static const PRODUCTLIST = _Paths.PRODUCTLIST;
  static const PRODUCT_DETAILED = _Paths.PRODUCT_DETAILED;
  static const MY_CART = _Paths.MY_CART;
  static const ORDER = _Paths.ORDER;
  static const MY_ORDER = _Paths.MY_ORDER;
  static const ORDER_DETAILS = _Paths.ORDER_DETAILS;
  static const MY_ACCOUNT = _Paths.MY_ACCOUNT;
  static const STORE_LOCATOR = _Paths.STORE_LOCATOR;
}

abstract class _Paths {
  _Paths._();
  static const LOGIN = '/login';
  static const FORGOT_PASSWORD = '/forgot-password';
  static const REGISTER = '/register';
  static const HOME = '/home';
  static const DRAWER = '/drawer';
  static const PRODUCTLIST = '/tables';
  static const PRODUCT_DETAILED = '/product-detailed';
  static const MY_CART = '/my-cart';
  static const ORDER = '/order';
  static const MY_ORDER = '/my-order';
  static const ORDER_DETAILS = '/order-details';
  static const MY_ACCOUNT = '/my-account';
  static const STORE_LOCATOR = '/store-locator';
}
