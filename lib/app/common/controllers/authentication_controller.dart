import 'dart:developer';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../routes/app_pages.dart';
import '../models/fetch_data_model.dart';
import '../repository/fetch_data_provider.dart';

class AuthenticationController extends GetxController {
  FetchUserData userData = FetchUserData();
  Rx<String?> accessToken = Rx(null);

//* this function will call as soon as controller intializes -------->>>>>.
  @override
  void onInit() async {
    ever(accessToken, landingScreen);

    await sharedTokenPreference();

    super.onInit();
  }

//* SharePreference value in accessToken -------->>>>>>>
  Future<void> sharedTokenPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    // accessToken.value = null;
    // log(preferences.getString("key").toString());

    accessToken.value = preferences.getString("accessToken");
  }

  //* condition for intial landing screen ------>>>>
  landingScreen(Object? token) async {
    log(token.toString());
    if (token == null) {
      Get.offAllNamed(Routes.LOGIN);
      log("user log out");
    } else {
      userData = await FetchDataProvider().fetchUser();

      Get.offAllNamed(Routes.DRAWER);
      log("user is now logged in");
    }
  }

  //* Get access token value ------->>>>>>
  String? get getToken => accessToken.value;

  //* Set access token value when Login or Logout ---->>>>>>>>>
  Future<void> setToken(String? token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (token == null) {
      prefs.remove("accessToken");
      accessToken.value = null;
    } else {
      prefs.setString("accessToken", token);
      accessToken.value = prefs.getString("accessToken");
    }
  }
}
