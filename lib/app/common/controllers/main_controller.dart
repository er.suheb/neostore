import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../models/fetch_data_model.dart';

class MainController extends GetxController {
  FetchUserData userModel = FetchUserData();

  //* widget will load untill it fetches the response from Server--------->>>>>

  Widget loadUntillFetch(
      BuildContext context, Widget child, ImageChunkEvent? loading) {
    if (loading == null) return child;
    return Container(
      color: Colors.grey.shade300,
      child: Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.red.shade700,
          color: Colors.black,
          value: loading.expectedTotalBytes != null
              ? loading.cumulativeBytesLoaded / loading.expectedTotalBytes!
              : null,
        ),
      ),
    );
  }
}
