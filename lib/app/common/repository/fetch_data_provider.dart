import 'dart:developer';

import 'package:get/get.dart';

import '../controllers/authentication_controller.dart';
import '../models/fetch_data_model.dart';

class FetchDataProvider extends GetConnect {
  Future<FetchUserData> fetchUser() async {
    final token = Get.find<AuthenticationController>().getToken!;

    final response = await get(
        "http://staging.php-dev.in:8844/trainingapp/api/users/getUserData",
        headers: {"access_token": token});
    log(response.body.toString());
    return fetchUserDataFromJson(response.body);
  }
}
