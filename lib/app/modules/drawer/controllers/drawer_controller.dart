import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import '../../../components/widgets/snackbar.dart';
import '../../../components/widgets/customText.dart';
import '../../order/views/order_list_view.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../../../common/models/fetch_data_model.dart';
import '../../../common/repository/fetch_data_provider.dart';
import '../../../routes/app_pages.dart';

class DrawerXController extends GetxController with StateMixin<dynamic> {
  FetchUserData userData = FetchUserData();
  File? pickedImage;

  void logout() {
    Get.find<AuthenticationController>().setToken(null);
  }

  //* Image picker Alert Dialog >>>>>>>>>
  Future<void> picImage(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13),
            ),
            title: CustomText(
              text: "Choose Option",
              color: Colors.red.shade800,
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                    },
                    title: CustomText(
                      text: "Gallery",
                      color: Colors.red.shade800,
                    ),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.red[800],
                    ),
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                    },
                    title: CustomText(
                      text: "Camera",
                      color: Colors.red.shade800,
                    ),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.red.shade800,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  //* Pic image from camera   ---->>>>>>>>>>>>
  void _openCamera(BuildContext context) async {
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
    );
    Get.back();
    if (pickedFile != null) {
      pickedImage = File(pickedFile.path);
    } else {
      pickedImage = null;
    }
    update();
  }

  //* Pic image from gallery >>>>>>>>>>>>
  void _openGallery(BuildContext context) async {
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
    );
    Get.back();
    if (pickedFile != null) {
      pickedImage = File(pickedFile.path);
    } else {
      pickedImage = null;
    }
    update();
  }

  @override
  void onInit() async {
    fetchUserData();

    super.onInit();
  }

  fetchUserData() async {
    change(null, status: RxStatus.loading());
    await Future.delayed(Duration(seconds: 3));
    userData = await FetchDataProvider().fetchUser();
    change(userData, status: RxStatus.success());
  }

  @override
  void onClose() {
    super.onClose();
  }

  void selectedMenu(String menuItem) {
    switch (menuItem) {
      case "My Cart":
        Get.toNamed(
          Routes.MY_CART,
        );
        break;
      case "Tables":
        Get.toNamed(Routes.PRODUCTLIST, arguments: 1);
        break;

      case "Sofas":
        Get.toNamed(Routes.PRODUCTLIST, arguments: 3);
        break;
      case "Chair":
        Get.toNamed(Routes.PRODUCTLIST, arguments: 2);
        break;
      case "Beds":
        Get.toNamed(Routes.PRODUCTLIST, arguments: 4);
        break;
      case "Log Out":
        Get.find<AuthenticationController>().setToken(null);
        break;

      case "My Order":
        Get.toNamed(Routes.MY_ORDER);
        break;

      case "Store Locator":
        Get.toNamed(Routes.STORE_LOCATOR);
        break;
      case "My Account":
        Get.toNamed(Routes.MY_ACCOUNT);
        break;
    }
  }
}
