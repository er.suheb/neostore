import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/config.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import '../../myAccount/controllers/my_account_controller.dart';
import '../../myAccount/views/update_profile_view.dart';
import '../../../routes/app_pages.dart';
import '../../../common/controllers/authentication_controller.dart';
import '../../../components/widgets/customText.dart';
import '../../../components/widgets/empty_avtar.dart';
import '../../../components/widgets/filled_avtar.dart';
import '../../home/views/home_view.dart';
import '../controllers/drawer_controller.dart';

class DrawerView extends GetView<DrawerXController> {
  DrawerView({
    Key? key,
  }) : super(key: key);
  MenuItem currentItem = MenuItems.tables;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DrawerXController>(
      builder: (_) {
        return ZoomDrawer(
          angle: 0,
          menuScreenWidth: 235,
          borderRadius: 5,
          slideWidth: 235,
          isRtl: false,
          // drawerShadowsBackgroundColor: Colors.white,
          menuBackgroundColor: Colors.black54,

          style: DrawerStyle.defaultStyle,
          // mainScreen: getMainScreen(screen),

          mainScreen: HomeView(),
          menuScreen: MenuPage(
              currentItem: currentItem,
              onSelectedItem: (item) {
                log(item.title.toString());
                controller.selectedMenu(item.title);
              }),
        );
      },
    );
  }
}

class MenuItems {
  static final cart = MenuItem(
    Icons.card_travel,
    'My Cart',
  );
  static final tables = MenuItem(
    Icons.table_bar,
    'Tables',
  );
  static final sofas = MenuItem(
    Icons.chair,
    'Sofas',
  );
  static final chairs = MenuItem(
    Icons.chair_alt,
    'Chair',
  );
  static final cups = MenuItem(
    Icons.bed,
    'Beds',
  );
  static final account = MenuItem(
    Icons.person,
    'My Account',
  );

  static final storelocator = MenuItem(
    Icons.where_to_vote,
    'Store Locator',
  );

  static final order = MenuItem(
    Icons.checklist,
    'My Order',
  );

  static final logout = MenuItem(
    Icons.exit_to_app,
    'Log Out',
  );
  static List<MenuItem> all = <MenuItem>[
    cart,
    tables,
    sofas,
    chairs,
    cups,
    account,
    storelocator,
    order,
    logout
  ];
}

class MenuItem {
  final String title;
  final IconData icon;
  MenuItem(
    this.icon,
    this.title,
  );
}

class MenuPage extends GetView<DrawerXController> {
  final MenuItem currentItem;
  final ValueChanged<MenuItem> onSelectedItem;

  const MenuPage({
    Key? key,
    required this.currentItem,
    required this.onSelectedItem,
  }) : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    return controller.obx(onLoading: LoadingDrawerMenu(context), (state) {
      return Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Spacer(),
              Center(
                child: GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.MY_ACCOUNT);
                  },
                  child: GetBuilder<DrawerXController>(
                    builder: ((controller) {
                      if (Get.find<DrawerXController>()
                              .userData
                              .data!
                              .userData!
                              .profilePic ==
                          null) {
                        return const EmptyImageAvatar();
                      } else {
                        return FilledImageAvatar(
                            imageUrl: Get.find<DrawerXController>()
                                .userData
                                .data!
                                .userData!
                                .profilePic!);
                      }
                    }),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Column(
                    children: [
                      Text(
                        "${controller.userData.data!.userData!.firstName!} ${controller.userData.data!.userData!.lastName}",
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Center(
                        child: Text(
                          controller.userData.data!.userData!.email!,
                          style: Theme.of(context).textTheme.headline5,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const Spacer(
                  // flex: 3,
                  ),
              ...MenuItems.all.map(buildMenuItem).toList(),
              SizedBox(
                height: 10,
              ),
              Spacer(
                flex: 2,
              ),
              const SizedBox(
                height: 40,
              )
            ],
          ),
        ),
      );
    });
  }

  Center LoadingDrawerMenu(BuildContext context) {
    return Center(
        child: Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Spacer(),
            const Center(
                child: CircleAvatar(
              backgroundColor: Colors.white12,
              minRadius: 48,
            )),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Column(
                  children: [
                    Container(
                      color: Colors.white30,
                      width: 30,
                      height: 7,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      color: Colors.white10,
                      width: 100,
                      height: 7,
                    ),
                  ],
                ),
              ),
            ),
            const Spacer(
                // flex: 3,
                ),
            ...MenuItems.all.map(buildMenuItem).toList(),
            SizedBox(
              height: 10,
            ),
            Spacer(
              flex: 2,
            ),
            const SizedBox(
              height: 40,
            )
          ],
        ),
      ),
    ));
  }

  Widget buildMenuItem(MenuItem item) => ListTile(
        minLeadingWidth: 20,
        hoverColor: Colors.red.shade900.withOpacity(0.5),
        // focusColor: Colors.red,
        // selectedTileColor: Colors.red.shade900.withOpacity(0.2),
        // selected: currentItem == item,
        onTap: () {
          onSelectedItem(item);
        },
        leading: Icon(
          item.icon,
          size: 22,
          color: Color.fromARGB(255, 231, 231, 231),
        ),
        title: Text(
          item.title,
          style: const TextStyle(
            color: Color.fromARGB(255, 231, 231, 231),
            fontFamily: 'GothamMedium',
            fontSize: 13,
          ),
        ),
        trailing: item.title == "My Cart"
            ? CircleAvatar(
                radius: 16,
                backgroundColor: Colors.red.shade900,
                child: CustomText(
                  text: (Get.find<AuthenticationController>()
                          .userData
                          .data!
                          .totalCarts!)
                      .toString(),
                ))
            : SizedBox.shrink(),
      );
}
