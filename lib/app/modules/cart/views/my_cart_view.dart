import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';

import '../../../common/controllers/main_controller.dart';
import '../../../components/widgets/ProgressIndicator.dart';
import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/customButton.dart';
import '../../../components/widgets/customText.dart';
import '../../../routes/app_pages.dart';
import '../controllers/cart_controller.dart';

//! Bug in this class need to fix --

class MyCartView extends GetView<MyCartController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: customAppBar(
          text: "My Cart",
          size: 16,
          action: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: Icon(Icons.search),
            )
          ],
          leading: InkWell(
            onTap: (() {
              Get.back();
            }),
            child: const Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
            ),
          ),
        ),
        body: controller.obx(
            onLoading: ProgressingIndicator(),
            onEmpty: Center(
              child: Column(
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.shopping_cart,
                          color: Colors.blueGrey.shade100,
                          size: 200,
                        ),
                        CustomText(
                          text: "Cart is Empty",
                          color: Colors.blueGrey.shade200,
                        )
                      ],
                    ),
                  ),
                  CustomButton(
                      navigation: () {
                        Get.offNamed(Routes.DRAWER);
                   
                      },
                      text: "Let's Shop",
                      textColor: Colors.white,
                      backgroundColor: Colors.red.shade800),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ), (state) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (_, index) {
                      return Slidable(
                        key: const ValueKey(0),

                        // The end action pane is the one at the right or the bottom side.
                        endActionPane: ActionPane(
                          extentRatio: 0.2,
                          motion: const ScrollMotion(),
                          children: [
                            SlidableAction(
                              onPressed: (context) {
                                controller.deleteCartProduct(index);
                              },
                              backgroundColor: Colors.red.shade800,
                              autoClose: true,
                              // spacing: 10,
                              foregroundColor: Colors.white,
                              icon: Icons.delete,
                              label: 'Delete',
                            ),
                          ],
                        ),

                        child: Container(
                          color: Colors.white,
                          child: Row(
                            children: [
                              Container(
                                padding: EdgeInsets.all(20),
                                width: 130,
                                child: Image.network(
                                  controller.cartProducts.data![index].product!
                                      .productImages
                                      .toString(),
                                  loadingBuilder: Get.find<MainController>()
                                      .loadUntillFetch,
                                  fit: BoxFit.contain,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomText(
                                    overflow: TextOverflow.ellipsis,
                                    text: controller.cartProducts.data![index]
                                        .product!.name!,
                                    color: Colors.black87,
                                    size: 16,
                                  ),
                                  SizedBox(height: 10),
                                  CustomText(
                                    text:
                                        "(${controller.cartProducts.data![index].product!.productCategory!})",
                                    color: Colors.grey.shade700,
                                    size: 13,
                                  ),
                                  DropdownButton<String>(
                                    borderRadius: BorderRadius.circular(5),
                                    dropdownColor: Colors.grey.shade100,
                                    value: controller
                                        .cartProducts.data![index].quantity
                                        .toString(),
                                    elevation: 1,
                                    style: TextStyle(
                                      color: Colors.grey.shade400,
                                    ),
                                    iconEnabledColor: Colors.black87,
                                    items: controller.productQuantity
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(
                                          value,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (currectValue) =>
                                        controller.editCartProductQuantity(
                                            index: index,
                                            quantity: int.parse(currectValue!)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (_, index) => Divider(
                          height: 0.1,
                          indent: 2,
                          thickness: 1,
                          color: Colors.grey.shade200,
                        ),
                    itemCount: controller.cartProducts.data!.length),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    // border: Border.all(color: Colors.grey.shade400),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                    )),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          CustomText(
                            text: "TOTAL",
                            color: Colors.black87,
                            fontName: "GothamMedium",
                          ),
                          CustomText(
                            text:
                                "₹ ${controller.cartProducts.total.toString()}",
                            color: Colors.black87,
                            size: 16,
                            fontName: "GothamBold",
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: CustomButton(
                          navigation: () => Get.toNamed(Routes.ORDER),
                          text: "Order Now",
                          textColor: Colors.white,
                          backgroundColor: Colors.red.shade800),
                    )
                  ],
                ),
              )
            ],
          );
        }));
  }
}
