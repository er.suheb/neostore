import 'dart:developer';

import 'package:get/get.dart';
import 'package:get/get_connect/connect.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../models/editCart_Model.dart';

class EditCartProvider extends GetConnect {
  final token = Get.find<AuthenticationController>().accessToken.toString();
  Future<EditCartModel> editCart(int prodcutID, int quantity) async {
    log(prodcutID.toString());
    FormData editCartFormData = FormData({
      "product_id": prodcutID,
      "quantity": quantity,
    });

    try {
      final response = await post(
          "http://staging.php-dev.in:8844/trainingapp/api/editCart",
          editCartFormData,
          headers: {
            "access_token": token,
          });
      log(response.body.toString());
      return editCartModelFromJson(response.body);
    } catch (e) {
      return EditCartModel(
          status: 502, userMsg: "Couldn't connect you to the server");
    }
  }
}
