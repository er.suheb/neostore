import 'dart:developer';

import 'package:get/get.dart';
import 'package:get/get_connect/connect.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../models/cart_item_model.dart';

class CartProvider extends GetConnect {
  Future<CartItemModel> fetchCartItem() async {
    final token = Get.find<AuthenticationController>().getToken!;
    final response = await get(
        "http://staging.php-dev.in:8844/trainingapp/api/cart",
        headers: {"access_token": token});
    log(response.body.toString());
    return cartItemResponseModelFromJson(response.body);
  }
}
