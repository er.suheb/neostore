import 'dart:developer';

import 'package:get/get.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../controllers/cart_controller.dart';
import '../models/deleteCart_item_model.dart';

class DeleteCartProvider extends GetConnect {
  Future<DeleteCartItemModel> deleteCartProvider({required int index}) async {
    FormData deleteCartFormData = FormData({
      "product_id":
          Get.find<MyCartController>().cartProducts.data![index].productId
    });

    final response = await post(
        "http://staging.php-dev.in:8844/trainingapp/api/deleteCart",
        deleteCartFormData,
        headers: {
          "access_token": Get.find<AuthenticationController>().getToken!
        });
    log(response.body);
    return deleteCartItemModelFromJson(response.body);
  }
}
