import 'dart:convert';

EditCartModel editCartModelFromJson(String str) =>
    EditCartModel.fromJson(json.decode(str));

String editCartModelToJson(EditCartModel data) => json.encode(data.toJson());

class EditCartModel {
  EditCartModel({
    this.status,
    this.data,
    this.totalCarts,
    this.message,
    this.userMsg,
  });

  int? status;
  bool? data;
  int? totalCarts;
  String? message;
  String? userMsg;

  factory EditCartModel.fromJson(Map<String, dynamic> json) => EditCartModel(
        status: json["status"],
        data: json["data"],
        totalCarts: json["total_carts"],
        message: json["message"],
        userMsg: json["user_msg"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data,
        "total_carts": totalCarts,
        "message": message,
        "user_msg": userMsg,
      };
}
