import 'dart:convert';

DeleteCartItemModel deleteCartItemModelFromJson(String str) =>
    DeleteCartItemModel.fromJson(json.decode(str));

String deleteCartItemToJson(DeleteCartItemModel data) =>
    json.encode(data.toJson());

class DeleteCartItemModel {
  DeleteCartItemModel({
    this.status,
    this.data,
    this.totalCarts,
    this.message,
    this.userMsg,
  });

  int? status;
  bool? data;
  int? totalCarts;
  String? message;
  String? userMsg;

  factory DeleteCartItemModel.fromJson(Map<String, dynamic> json) =>
      DeleteCartItemModel(
        status: json["status"],
        data: json["data"],
        totalCarts: json["total_carts"],
        message: json["message"],
        userMsg: json["user_msg"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data,
        "total_carts": totalCarts,
        "message": message,
        "user_msg": userMsg,
      };
}
