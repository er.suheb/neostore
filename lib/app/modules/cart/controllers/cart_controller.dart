import 'package:get/get.dart';

import '../../../components/widgets/snackbar.dart';
import '../models/cart_item_model.dart';
import '../models/deleteCart_item_model.dart';
import '../models/editCart_Model.dart';
import '../provider/cart_provider.dart';
import '../provider/deleteCart_item_provider.dart';
import '../provider/editCart_provider.dart';

class MyCartController extends GetxController with StateMixin<dynamic> {
  CartItemModel cartProducts = CartItemModel();
  List<String> productQuantity = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
  ];

  @override
  void onInit() async {
    await fetchCartProducts();
    super.onInit();
  }

//* this will fetch all the product in to cart as list ---------->>>>>>
  Future<void> fetchCartProducts() async {
    change(null, status: RxStatus.loading());
    cartProducts = await CartProvider().fetchCartItem();
    if (cartProducts.status == 200) {
      if(cartProducts.data!=null){
        change(cartProducts, status: RxStatus.success());
      }else{
        change(null,status: RxStatus.empty());
      }
      
    } else {
      change("Couldn't connect you with server", status: RxStatus.error());
    }
  }

//* this will edit quantity of product on added product on cart -------->>>>
  void editCartProductQuantity(
      {required int index, required int quantity}) async {
    change(null, status: RxStatus.loading());
    EditCartModel cartItemProductQuantity = await EditCartProvider()
        .editCart(cartProducts.data![index].productId!, quantity);
    change(cartItemProductQuantity, status: RxStatus.success());

    if (cartItemProductQuantity.status == 200) {
      showGreenSnackbar(
          text: "${cartItemProductQuantity.userMsg}",
          massage: "You can order now");
      fetchCartProducts();
    } else {
      showRedSnackbar("${cartItemProductQuantity.userMsg}",
          "There is some issue with the server");
    }
  }

//* this will delete selected cart item ---------- >>>>>>>>>>>>>.

  deleteCartProduct(int index) async {
    change(null, status: RxStatus.loading());
    DeleteCartItemModel deleteCartItemResponse =
        await DeleteCartProvider().deleteCartProvider(index: index);
    change(deleteCartItemResponse, status: RxStatus.success());
    if (deleteCartItemResponse.status == 200) {
      showGreenSnackbar(
          text: "Item Removed", massage: "${deleteCartItemResponse.userMsg}");
    } else {
      showRedSnackbar("${deleteCartItemResponse.status}",
          "${deleteCartItemResponse.userMsg}");
    }
  }
}
