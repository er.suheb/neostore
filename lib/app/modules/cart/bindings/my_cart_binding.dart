import 'package:get/get.dart';

import '../controllers/cart_controller.dart';

class MyCartBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MyCartController>(
      () => MyCartController(),
    );
  }
}
