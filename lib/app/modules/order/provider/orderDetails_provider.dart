import 'dart:developer';

import 'package:get/get.dart';
import '../../../common/controllers/authentication_controller.dart';
import '../models/orderDetails_models.dart';

class OrderDetailsProvider extends GetConnect {
  Future<OrderDetailResponseModel> getOrderDetails(
      {required int orderId}) async {
    try {
      final response = await get(
        "http://staging.php-dev.in:8844/trainingapp/api/orderDetail",
        headers: {
          "access_token": Get.find<AuthenticationController>().getToken!,
        },
        query: {"order_id": orderId.toString()},
      );

      log(response.body.toString());
      return orderDetailResponseModelFromJson(response.body);
    } catch (e) {
      log("Error on get order details : $e");
      return OrderDetailResponseModel(status: 502);
    }
  }
}
