import 'dart:developer';

import 'package:get/get.dart';
import '../models/orderDetails_models.dart';
import '../../../common/controllers/authentication_controller.dart';

import '../models/order_model.dart';

class OrderProvider extends GetConnect {
  Future<PlaceOrderResponseModel> placeOrder(
      {required final String address}) async {
    try {
      final response = await post(
        "http://staging.php-dev.in:8844/trainingapp/api/order",
        FormData({"address": address}),
        headers: {
          "access_token": Get.find<AuthenticationController>().getToken!
        },
      );
      log(response.body.toString());
      return placeOrderResponseModelFromJson(response.body);
    } catch (e) {
      return PlaceOrderResponseModel(
          status: 502, userMsg: "Something went wrong");
    }
  }
}
