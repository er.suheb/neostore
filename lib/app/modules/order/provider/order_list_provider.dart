import 'dart:developer';

import 'package:get/get.dart';
import 'package:get/get_connect/connect.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../models/order_list_model.dart';

class OrderListProvider extends GetConnect {
  Future<OrderListModel> fetchallMyOrders() async {
    try {
      final response = await get(
          "http://staging.php-dev.in:8844/trainingapp/api/orderList",
          headers: {
            "access_token": Get.find<AuthenticationController>().getToken!
          });
      log("My Order response is ::: ${response.body.toString()}");
      return orderListModelFromJson(response.body);
    } catch (e) {
      return OrderListModel(userMsg: "Server Error", status: 502);
    }
  }
}
