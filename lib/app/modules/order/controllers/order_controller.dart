import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../drawer/controllers/drawer_controller.dart';

import '../../../components/widgets/snackbar.dart';
import '../../cart/controllers/cart_controller.dart';
import '../models/orderDetails_models.dart';
import '../models/order_list_model.dart';
import '../provider/orderDetails_provider.dart';
import '../provider/order_list_provider.dart';
import '../provider/order_porivder.dart';
import '../views/orderDeatils_view.dart';

class OrderController extends GetxController with StateMixin<dynamic> {
  final addressFormKey = GlobalKey<FormState>();
  OrderListModel myOrderResponse = OrderListModel();
  OrderDetailResponseModel orderDetailModel = OrderDetailResponseModel();
  // int currentOrderId = 0;
  late int myOrderId;

  //* Text editing controllers ---->>>>>>>>>>>>>
  TextEditingController addressController = TextEditingController();
  TextEditingController cityLandmarkController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController zipCodeController = TextEditingController();
  TextEditingController countryController = TextEditingController();

  //* form validation  --------->>>>>>>>>>

  String? validateAddress(String? value) {
    if (value!.isEmpty || value.length < 10) {
      return "Address must be atleast 20 characters";
    } else
      return null;
  }

  String? validateCity(String? value) {
    if (value!.isEmpty || value.length < 5) {
      return "City must be atleast 5 characters";
    } else
      return null;
  }

  String? validateState(String? value) {
    if (value!.isEmpty || value.length < 5) {
      return "State must be atleast 5 characters";
    } else
      return null;
  }

  String? validateZipCode(String? value) {
    if (value!.isEmpty || value.length < 6) {
      return "Zip Code must be atleast 6 letters ";
    } else
      return null;
  }

  String? validateCountry(String? value) {
    if (value!.isEmpty || value.length < 4) {
      return "Country must be atleast 4 charaters";
    } else
      return null;
  }

  @override
  void onInit() async {
    // change(null, status: RxStatus.loading());
    await getMyOrder();
    // change(null, status: RxStatus.success());
    // getOrderDetail(currentOrderId);
    super.onInit();
  }

  Future<void> getMyOrder() async {
    myOrderResponse = await OrderListProvider().fetchallMyOrders();
    if (myOrderResponse.status == 200) {
      if (myOrderResponse.data == null) {
        change(null, status: RxStatus.empty());
      }
      change(myOrderResponse, status: RxStatus.success());
    } else {
      change(null, status: RxStatus.error());
    }
  }

  void placeOrder() async {
    if (addressFormKey.currentState!.validate()) {
      Get.back();
      change(null, status: RxStatus.loading());
      final addressString =
          "${addressController.text}, ${cityLandmarkController.text}, ${cityController.text}, ${stateController.text}, ${zipCodeController.text}, ${countryController.text}.";

      final orderResponse =
          await OrderProvider().placeOrder(address: addressString);

      change(orderResponse, status: RxStatus.success());

      if (orderResponse.status == 200) {
        showGreenSnackbar(
            text: "Order placed successfully",
            massage: "You order will be at your door step");
      } else {
        showRedSnackbar("Order Failed", "Try again");
      }

      Get.find<MyCartController>().fetchCartProducts();
    }
  }

//* orderDetail data fetch ------------->>>>>>
  Future<void> getOrderDetails({required int orderId}) async {
    myOrderId = orderId;

    Get.to(() => OrderDetails());
    change(null, status: RxStatus.loading());
    orderDetailModel =
        await OrderDetailsProvider().getOrderDetails(orderId: orderId);
    if (orderDetailModel.status == 200) {
      if (orderDetailModel.data == null) {
        change(null, status: RxStatus.empty());
      } else {
        change(orderDetailModel, status: RxStatus.success());
      }
    } else {
      change(null, status: RxStatus.error());
    }
  }

  // void gotoDetails(int orderId) async {
  //   currentOrderId = orderId;
  //   print(currentOrderId);
  //   await getOrderDetail(orderId: orderId);
  //   // Get.toNamed(Routes.ORDER_DETAILS);
  // }
}
