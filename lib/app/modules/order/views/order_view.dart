import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/customButton.dart';
import '../../../components/widgets/customTextField.dart';
import '../controllers/order_controller.dart';

class OrderView extends GetView<OrderController> {
  const OrderView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey.shade300,
        appBar: customAppBar(text: "Add Address"),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(40),
            child: Form(
              key: controller.addressFormKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomTextField(
                    isDense: false,
                    focusedBorder: Colors.red.shade900,
                    errorColor: Colors.red.shade500,
                    borderColor: Colors.grey,
                    fillColor: Colors.white,
                    textColor: Colors.black87,
                    validator: controller.validateAddress,
                    controller: controller.addressController,
                    maxLines: 3,
                    labelText: "ADDRESS*",
                  ),
                  SizedBox(height: 20),
                  CustomTextField(
                    isDense: false,
                    focusedBorder: Colors.red.shade900,
                    borderColor: Colors.grey, errorColor: Colors.red.shade500,
                    fillColor: Colors.white, textColor: Colors.black87,
                    controller: controller.cityLandmarkController,
                    maxLines: 1,
                    labelText: "Nearby location",
                    // hintText: "LANDMARK",
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                        child: CustomTextField(
                          isDense: false,
                          focusedBorder: Colors.red.shade900,
                          errorColor: Colors.red.shade500,
                          borderColor: Colors.grey,
                          fillColor: Colors.white,
                          textColor: Colors.black87,
                          validator: controller.validateCity,
                          controller: controller.cityController,
                          labelText: "CITY*",
                        ),
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Expanded(
                        child: CustomTextField(
                          isDense: false,
                          focusedBorder: Colors.red.shade900,
                          borderColor: Colors.grey,
                          errorColor: Colors.red.shade500,
                          fillColor: Color.fromRGBO(255, 255, 255, 1),
                          textColor: Colors.black87,
                          validator: controller.validateState,
                          controller: controller.stateController,
                          labelText: "STATE*",
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                        child: CustomTextField(
                          isDense: false,
                          focusedBorder: Colors.red.shade900,
                          borderColor: Colors.grey,
                          errorColor: Colors.red.shade500,
                          fillColor: Colors.white,
                          textColor: Colors.black87,
                          validator: controller.validateZipCode,
                          controller: controller.zipCodeController,
                          labelText: "ZIP CODE*",
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Expanded(
                        child: CustomTextField(
                          isDense: false,
                          focusedBorder: Colors.red.shade900,
                          borderColor: Colors.grey,
                          errorColor: Colors.red.shade500,
                          fillColor: Colors.white,
                          textColor: Colors.black87,
                          validator: controller.validateCountry,
                          controller: controller.countryController,
                          labelText: "COUNTRY*",
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 40),
                  CustomButton(
                      navigation: () => controller.placeOrder(),
                      text: "Place Order",
                      textColor: Colors.white,
                      backgroundColor: Colors.red.shade700)
                ],
              ),
            ),
          ),
        )

        // onLoading: ProgressIndicator(
        //   // loadingText: "Please wait...",
        // ),
        // onError: (_) => OnError(
        //   errText: "Something went wrong",
        //   onRefresh: () {
        //     Get.back();
        //     Get.find<CartController>().fetchCartProducts();
        //   },
        // ),

        );
  }
}
