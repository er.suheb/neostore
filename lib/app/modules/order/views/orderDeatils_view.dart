import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

import '../../../components/widgets/ProgressIndicator.dart';
import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/customText.dart';
import '../controllers/order_controller.dart';

class OrderDetails extends GetView<OrderController> {
  late int index;
  OrderDetails({Key? key}) : super(key: key);

  @override
  Widget build(
    BuildContext context,
  ) {
    return controller.obx(onLoading: ProgressingIndicator(), (state) {
      return Scaffold(
        appBar: customAppBar(
          text: "Order Id : ${controller.myOrderId}",
          size: 16,
          action: [Icon(Icons.search)],
          leading: InkWell(
            onTap: (() {
              Get.back();
            }),
            child: Icon(
              Icons.arrow_back,
            ),
          ),
        ),
        body: controller.obx(
          (state) {
            return ListView.separated(
                itemBuilder: (_, index) {
                  if (index ==
                      controller.orderDetailModel.data!.orderDetails!.length) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          CustomText(
                            text: "TOTAL",
                            color: Colors.black87,
                          ),
                          CustomText(
                            text: controller.orderDetailModel.data!
                                .orderDetails![index].total
                                .toString(),
                            color: Colors.black87,
                          ),
                        ],
                      ),
                    );
                  } else {
                    return ListTile(
                      title: CustomText(
                        text:
                            "${controller.orderDetailModel.data!.orderDetails![index].prodName}",
                        color: Colors.black87,
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            // text: "",
                            text:
                                " (${controller.orderDetailModel.data!.orderDetails![index].prodCatName})",

                            color: Colors.black87,
                          ),
                          SizedBox(height: 10),
                          CustomText(
                            text:
                                "QTY : 3 ${controller.orderDetailModel.data!.orderDetails![index].quantity}",
                            color: Colors.black87,
                          ),
                        ],
                      ),
                      trailing: CustomText(
                        text: "₹ ${controller.orderDetailModel.data!.cost}",
                        color: Colors.black87,
                      ),
                      leading: Container(
                        color: Colors.grey.shade100,
                        padding: const EdgeInsets.all(10),
                        width: 100,
                        child: Image.network(
                            "${controller.orderDetailModel.data!.orderDetails![index].prodImage}"),
                      ),
                    );
                  }
                },
                separatorBuilder: (_, index) {
                  return Divider(
                    color: Colors.black54,
                  );
                },
                itemCount:
                    controller.orderDetailModel.data!.orderDetails!.length);
          },
        ),
      );
    });
  }
}
