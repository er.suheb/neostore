import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../../components/widgets/ProgressIndicator.dart';
import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/customButton.dart';
import '../../../components/widgets/customText.dart';
import '../controllers/order_controller.dart';

class OrderListView extends GetView<OrderController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(
        text: "NeoSTORE",
        size: 16,
        action: [Icon(Icons.search)],
        leading: InkWell(
          onTap: (() {
            Get.back();
          }),
          child: Icon(
            Icons.arrow_back,
          ),
        ),
      ),
      body: controller.obx(
          onLoading: ProgressingIndicator(),
          onEmpty: CustomButton(
            text: "Buy your first product with us",
            backgroundColor: Colors.red.shade700,
            textColor: Colors.white,
          ),
          onError: (p0) => CustomText(
              text: "We can't connect you to the server try after a while"),
          (items) {
        // final fetchingMyOrders = items as MyOrderModel;

        return ListView.separated(
            itemBuilder: (_, index) {
              return ListTile(
                onTap: () => controller.getOrderDetails(
                    orderId: controller.myOrderResponse.data![index].id!),
                title: CustomText(
                  text:
                      "Order id ${controller.myOrderResponse.data![index].id}",
                  color: Colors.black,
                ),
                dense: true,
                subtitle: CustomText(
                  text:
                      "Ordered Date : ${controller.myOrderResponse.data![index].created}",
                  color: Colors.black54,
                  fontName: "Gotham",
                ),
                trailing: CustomText(
                  text: "₹ ${controller.myOrderResponse.data![index].cost}.00",
                  color: Colors.black,
                ),
              );
            },
            separatorBuilder: (_, index) {
              return Divider(
                color: Colors.grey.shade900,
              );
            },
            itemCount: controller.myOrderResponse.data!.length);
      }),
    );
  }
}
