import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import '../../../components/widgets/customButton.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../common/controllers/main_controller.dart';
import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/homeGridView.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final mainController = Get.find<MainController>();
  HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(
        text: "NeoSTORE",
        size: 16,
        action: [Icon(Icons.search)],
        leading: InkWell(
          onTap: (() {
            ZoomDrawer.of(context)!.toggle();
          }),
          child: Icon(
            Icons.menu,
          ),
        ),
      ),
      body: Column(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              CarouselSlider.builder(
                itemCount: controller.urlImages.length,
                itemBuilder: (context, index, realIndex) {
                  final urlimage = controller.urlImages[index];
                  return buildImage(urlimage, index);
                },
                options: CarouselOptions(
                    height: 250,
                    autoPlay: true,
                    viewportFraction: 1,
                    onPageChanged: (index, reason) =>
                        controller.activeIndex.value = index,
                    autoPlayInterval: Duration(seconds: 2)),
              ),
              Obx(
                () => Positioned(
                  bottom: 10,
                  child: Align(
                      alignment: Alignment.bottomRight,
                      child: AnimatedSmoothIndicator(
                        effect: SlideEffect(
                            strokeWidth: 2,
                            dotHeight: 10,
                            dotWidth: 10,
                            activeDotColor: Colors.black87,
                            dotColor: Colors.red.shade800),
                        count: controller.urlImages.length,
                        activeIndex: controller.activeIndex.value,
                      )),
                ),
              ),
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: GridView(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10),
                  children: [
                    GridContainer1(),
                    GridContainer2(),
                    GridContainer3(),
                    GridContainer4()
                  ]),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildImage(
    String urlimage,
    int index,
  ) =>
      Container(
        // margin: EdgeInsets.all(2),
        color: Colors.red[900],
        child: Image.network(
          urlimage,
          fit: BoxFit.cover,
          loadingBuilder: mainController.loadUntillFetch,
        ),
      );
}
