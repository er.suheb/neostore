import 'dart:developer';

import 'package:get/get.dart';

import '../models/productList_Model.dart';
import '../provider/product_list_provider.dart';

class ProductListController extends GetxController with StateMixin<dynamic> {
  late int productCatagoryId;
  ProductsListModel products = ProductsListModel();
  //TODO: Implement TablesController

  List<ProductData> tableContext = [
    ProductData(
        name: "Stylish Dining Tables",
        producer: "Aron Table Center",
        cost: 3,
        rating: 4,
        productImages: "https://picsum.photos/200"),
    ProductData(
        name: "Stylish",
        producer: "Aron Table Center",
        cost: 2,
        rating: 4,
        productImages: "https://picsum.photos/200"),
    ProductData(
        name: "6 Seater Dining Table",
        producer: "Aron Table Center",
        cost: 2,
        rating: 4,
        productImages: "https://picsum.photos/200"),
    ProductData(
        name: "Harkness Table for Offices",
        producer: "Tecnozip",
        cost: 1,
        rating: 4,
        productImages: "https://picsum.photos/200"),
    ProductData(
        name: "Style",
        producer: "Kifayti Shop",
        cost: 2344,
        rating: 4,
        productImages: "https://picsum.photos/200"),
  ];

  @override
  void onInit() async {
    productCatagoryId = Get.arguments as int;
    log(productCatagoryId.toString());
    change(null, status: RxStatus.loading());

    products = await ProductListProvider().fetchProductList(productCatagoryId);
    change(products, status: RxStatus.success());

    log(products.data!.length.toString());
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
