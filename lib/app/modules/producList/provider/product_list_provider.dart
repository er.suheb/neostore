import 'dart:developer';

import 'package:get/get_connect/connect.dart';

import '../models/productList_Model.dart';

class ProductListProvider extends GetConnect {
  Future<ProductsListModel> fetchProductList(int id) async {
    final response = await get(
        "http://staging.php-dev.in:8844/trainingapp/api/products/getList",
        query: {"product_category_id": id.toString()}
        // headers: {"product_category_id": id.toString()},
        );

    log(response.body);
    return productsListModelFromJson(response.body);
  }
}
// 