import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../../../common/controllers/main_controller.dart';
import '../../../components/widgets/ProgressIndicator.dart';
import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/customText.dart';
import '../../../components/widgets/starRating.dart';
import '../../../routes/app_pages.dart';
import '../controllers/productList_controller.dart';

class ProductListView extends GetView<ProductListController> {
  final fetchData = Get.find<AuthenticationController>();
  final mainController = Get.find<MainController>();
  @override
  Widget build(
    BuildContext context,
  ) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: customAppBar(
        text: fetchData.userData.data!
            .productCategories![controller.productCatagoryId - 1].name!,
        size: 16,
        action: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Icon(Icons.search),
          )
        ],
        leading: InkWell(
          onTap: (() {
            Get.offNamed(Routes.DRAWER);
          }),
          child: const Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Icon(
              Icons.arrow_back_ios,
              size: 15,
            ),
          ),
        ),
      ),
      body: ProductList(),
    );
  }
}

//* Main Product List Body -------->>>>>>>>>

class ProductList extends GetView<ProductListController> {
  ProductList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(onLoading: ProgressingIndicator(), (state) {
      return ListView.builder(
        itemCount: controller.products.data!.length,
        itemBuilder: (_, index) {
          return FittedBox(
            fit: BoxFit.contain,
            child: InkWell(
              onTap: () {
                Get.toNamed(Routes.PRODUCT_DETAILED,
                    arguments: Get.find<ProductListController>()
                        .products
                        .data![index]
                        .id);
              },
              child: Card(
                elevation: 0,
                child: Row(
                  children: [
                    FittedBox(
                      fit: BoxFit.cover,
                      child: ImageContainer(
                        index: index,
                      ),
                    ),
                    ProductContext(index: index),
                  ],
                ),
              ),
            ),
          );
        },
      );
    });
  }
}

//* Product context Widget ------------>>>>>>>>>>

class ProductContext extends GetView<ProductListController> {
  final int index;

  ProductContext({
    required this.index,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            text: controller.products.data![index].name!,
            color: Colors.black,
            size: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: CustomText(
              text: controller.products.data![index].producer!,
              color: Colors.grey.shade700,
              size: 13,
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: CustomText(
                  text: controller.products.data![index].cost!.toString(),
                  color: Colors.red.shade600,
                  fontName: "GothamBold",
                  size: 16,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 100.0, right: 10),
                child: AbsorbPointer(
                  child: StarRating(
                    rating: controller.products.data![index].rating!.toDouble(),
                    iconSize: 20,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

//* product Image method ---------->>>>>.
Padding ImageContainer({double? height, double? width, required int index}) {
  final mainController = Get.find<MainController>();
  final controller = Get.find<ProductListController>();
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      height: height,
      width: width,
      color: Colors.grey,
      child: Image.network(
        controller.products.data![index].productImages!,
        scale: 4.5,
        fit: BoxFit.cover,
        loadingBuilder: mainController.loadUntillFetch,
      ),
    ),
  );
}
