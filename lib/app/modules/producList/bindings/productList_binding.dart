import 'package:get/get.dart';

import '../controllers/productList_controller.dart';

class TablesBinding extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<ProductListController>(
    //   () => ProductListController(),
    // );
    Get.put(ProductListController());
  }
}
