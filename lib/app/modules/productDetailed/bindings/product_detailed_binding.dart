import 'package:get/get.dart';

import '../controllers/product_detailed_controller.dart';

class ProductDetailedBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProductDetailedController>(
      () => ProductDetailedController(),
    );
  }
}
