import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../../../common/controllers/main_controller.dart';
import '../../../components/widgets/ProgressIndicator.dart';
import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/buyNowDialog.dart';
import '../../../components/widgets/customButton.dart';
import '../../../components/widgets/customText.dart';
import '../../../components/widgets/ratingDialog.dart';
import '../../../components/widgets/starRating.dart';
import '../../../routes/app_pages.dart';
import '../../producList/controllers/productList_controller.dart';
import '../controllers/product_detailed_controller.dart';

class ProductDetailedView extends GetView<ProductDetailedController> {
  final mainController = Get.find<MainController>();
  @override
  Widget build(BuildContext context) {
    // int index;
    return controller.obx(onLoading: ProgressingIndicator(), (state) {
      return Scaffold(
          backgroundColor: Colors.grey.shade200,
          appBar: customAppBar(
            text: controller.productDetails.data!.name!,
            size: 16,
            action: [
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Icon(Icons.search),
              )
            ],
            leading: InkWell(
              onTap: (() => Get.offNamed(Routes.PRODUCTLIST)),
              child: const Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 15,
                ),
              ),
            ),
          ),
          body: Column(
            children: [
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        text: controller.productDetails.data!.name!,
                        size: 23,
                        fontName: "GothamBold",
                        color: Colors.black87,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 1.5),
                        child: CustomText(
                          text:
                              "Catagory - ${Get.find<AuthenticationController>().userData.data!.productCategories![Get.find<ProductListController>().productCatagoryId - 1].name!}",
                          size: 17,
                          fontName: "GothamLight",
                          color: Colors.black87,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomText(
                            text: controller.productDetails.data!.producer!,
                            size: 12,
                            fontName: "GothamLight",
                            color: Colors.black87,
                          ),
                          AbsorbPointer(
                            child: StarRating(
                              rating: controller.productDetails.data!.rating!
                                  .toDouble(),
                              iconSize: 15,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomText(
                                text:
                                    "Rs.${controller.productDetails.data!.cost.toString()}",
                                color: Colors.red.shade600,
                                size: 20,
                              ),
                              Icon(Icons.share),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            width: MediaQuery.of(context).size.width - 100,
                            height: 180,
                            child: GetBuilder<ProductDetailedController>(
                              builder: (_) {
                                return Image.network(
                                  controller
                                      .productDetails
                                      .data!
                                      .productImages![controller.indexImage]
                                      .image!,
                                  fit: BoxFit.contain,
                                  loadingBuilder:
                                      mainController.loadUntillFetch,
                                );
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          // flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(25.0),
                            child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              itemCount: controller
                                  .productDetails.data!.productImages!.length,
                              itemBuilder: (state, index) {
                                return InkWell(
                                  onTap: () => controller.updateImage(index),
                                  child: GetBuilder<ProductDetailedController>(
                                    builder: (_) {
                                      return Container(
                                        padding: EdgeInsets.all(5),
                                        width: 200,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                          color: controller.indexImage == index
                                              ? Colors.red.shade800
                                              : Colors.grey.shade200,
                                        )),
                                        child: Padding(
                                          padding: const EdgeInsets.all(0.0),
                                          child: Image.network(
                                            controller.productDetails.data!
                                                .productImages![index].image!,
                                            fit: BoxFit.contain,
                                            loadingBuilder:
                                                mainController.loadUntillFetch,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return SizedBox(
                                  width: 10,
                                );
                              },
                            ),
                          ),
                        ),
                        const Divider(),
                        Expanded(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Row(
                                  children: [
                                    CustomText(
                                      text: "DESCRIPTION",
                                      color: Colors.black87,
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15.0, vertical: 10),
                                child: SingleChildScrollView(
                                  child: CustomText(
                                    maxLines: 7,
                                    // overflow: TextOverflow.ellipsis,
                                    text: controller
                                        .productDetails.data!.description!,
                                    fontName: "Gotham",
                                    size: 12,
                                    color: Colors.black54,
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white),
                  width: MediaQuery.of(context).size.width - 30,
                  height: 60,
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomButton(
                            navigation: () => Get.dialog(BuyNowDialog(
                                  productDetails: controller.productDetails,
                                  imageIndex: controller.indexImage,
                                  onPressed: () => controller.buyNow(),
                                )),
                            verticalPadding: 5,
                            horizontalPadding: 5,
                            text: "BUY NOW",
                            textColor: Colors.white,
                            backgroundColor: Colors.red.shade700),
                      ),
                      Expanded(
                        child: CustomButton(
                            navigation: () {
                              Get.dialog(RatingDialog(controller: controller));
                            },
                            verticalPadding: 5,
                            horizontalPadding: 5,
                            text: "RATE",
                            textColor: Colors.black54,
                            backgroundColor: Colors.grey.shade300),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ));
    });
  }
}
