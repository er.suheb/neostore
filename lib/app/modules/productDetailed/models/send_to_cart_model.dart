import 'dart:convert';

SendToCartModel addToCartModelFromJson(String str) =>
    SendToCartModel.fromJson(json.decode(str));

String addToCartModelToJson(SendToCartModel data) => json.encode(data.toJson());

class SendToCartModel {
  SendToCartModel({
    this.status,
    this.data,
    this.totalCarts,
    this.message,
    this.userMsg,
  });

  int? status;
  bool? data;
  int? totalCarts;
  String? message;
  String? userMsg;

  factory SendToCartModel.fromJson(Map<String, dynamic> json) => SendToCartModel(
        status: json["status"],
        data: json["data"],
        totalCarts: json["total_carts"],
        message: json["message"],
        userMsg: json["user_msg"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data,
        "total_carts": totalCarts,
        "message": message,
        "user_msg": userMsg,
      };
}
