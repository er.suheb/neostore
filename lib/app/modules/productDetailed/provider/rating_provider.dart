import 'dart:developer';

import 'package:get/get.dart';

import '../models/rating_model.dart';

class RatingProvider extends GetConnect {
  Future<RatingModel> postRating(
      {required String productID, required String rating}) async {
    FormData ratingFormData = FormData({
      "product_id": productID,
      "rating": rating.toString(),
    });

    try {
      final response = await post(
          "http://staging.php-dev.in:8844/trainingapp/api/products/setRating",
          ratingFormData);
      log(response.body.toString());
    

      return ratingModelFromJson(response.body);
    } catch (e) {
      return RatingModel(
          message: e.toString(),
          status: 502,
          userMsg: "Couldn't get the server");
    }
  }
}
