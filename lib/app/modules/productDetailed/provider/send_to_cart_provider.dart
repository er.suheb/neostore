import 'dart:developer';

import 'package:get/get.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../models/send_to_cart_model.dart';

class SendToCartProvider extends GetConnect {
  Future<SendToCartModel> addToCart({
    required final int productId,
    required final int quantity,
  }) async {
    FormData data = FormData({
      "product_id": productId,
      "quantity": quantity,
    });

    try {
      final response = await post(
        "http://staging.php-dev.in:8844/trainingapp/api/addToCart",
        data,
        headers: {
          "access_token": Get.find<AuthenticationController>().getToken!,
        },
      );
      log(response.body.toString());
      return addToCartModelFromJson(response.body);
    } catch (e) {
      return SendToCartModel(
        userMsg: "Something went wrong",
        status: 502,
        message: e.toString(),
      );
    }
  }
}
