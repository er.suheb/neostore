import 'package:get/get.dart';
import 'package:get/get_connect/connect.dart';

import '../models/product_detailed_model.dart';

class ProductDetailProvider extends GetConnect {
  Future<ProductDetails> fetchProductDetails(int id) async {
    final response = await get(
        "http://staging.php-dev.in:8844/trainingapp/api/products/getDetail",
        query: {"product_id": id.toString()});
    print("Product Details responce : ${response.body}");
    return productDetailsFromJson(response.body);
  }
}
