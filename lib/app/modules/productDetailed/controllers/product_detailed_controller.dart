import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/widgets/snackbar.dart';
import '../../../routes/app_pages.dart';
import '../models/product_detailed_model.dart';
import '../models/send_to_cart_model.dart';
import '../provider/product_detailed_provider.dart';
import '../provider/rating_provider.dart';
import '../provider/send_to_cart_provider.dart';

class ProductDetailedController extends GetxController
    with StateMixin<dynamic> {
  TextEditingController quantityCtrl = TextEditingController(text: "1");
  int indexImage = 0;
  ProductDetails productDetails = ProductDetails();
  int productDetailId = 0;

  @override
  void onInit() async {
    productDetailId = Get.arguments;
    change(null, status: RxStatus.loading());
    await Future.delayed(Duration(seconds: 3));
    productDetails =
        await ProductDetailProvider().fetchProductDetails(productDetailId);

    change(productDetails, status: RxStatus.success());

    log(productDetails.data.toString());
    super.onInit();
  }

  void buyNow() async {
    Get.back();
    if (quantityCtrl.text.isEmpty || int.parse(quantityCtrl.text) < 1) {
      FocusManager.instance.primaryFocus?.unfocus();
      showRedSnackbar("Ooopss!!", "Invalid");
    } else {
      FocusManager.instance.primaryFocus?.unfocus();
      change(null, status: RxStatus.loading());

      // await Future.delayed(Duration(seconds: 5));

      SendToCartModel addToCartResponse = await SendToCartProvider().addToCart(
          productId: productDetails.data!.id!,
          quantity: int.parse(quantityCtrl.text));

      if (addToCartResponse.status == 200) {
        change(productDetails, status: RxStatus.success());
        Get.toNamed(Routes.MY_CART);

        showGreenSnackbar(
            text:
                "${(addToCartResponse.userMsg) ?? 'Added to cart successful'} | Total cart items : ${addToCartResponse.totalCarts}",
            massage: "Check Your Cart");
      } else {
        change(productDetails, status: RxStatus.success());
        showRedSnackbar(
            "${(addToCartResponse.userMsg) ?? "Something went wrong"} | Status : ${addToCartResponse.status}",
            "Please try again Buy");
      }
    }
  }

  void rateNow() async {
    Get.back();
    final ratingResponse = await RatingProvider().postRating(
        productID: productDetails.data!.id.toString(),
        rating: productDetails.data!.rating!.toString());
    if (ratingResponse.status == 200) {
      showGreenSnackbar(
          text: ratingResponse.userMsg.toString(),
          massage: "Product rating saved");
    } else {
      showRedSnackbar(ratingResponse.userMsg.toString(), "Try again rating");
    }
    change(ratingResponse, status: RxStatus.success());
  }

  updateImage(int index) {
    indexImage = index;
    update();
  }
}
