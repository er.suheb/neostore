import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../components/widgets/customText.dart';
import '../../../components/widgets/snackbar.dart';

import '../../drawer/controllers/drawer_controller.dart';
import '../model/reset_password_model.dart';
import '../provider/my_account_provider.dart';

class MyAccountController extends GetxController with StateMixin<dynamic> {
  final resetPasswordFormKey = GlobalKey<FormState>();
  final editProfieKey = GlobalKey<FormState>();
  File? pickedImage;

  @override
  void onInit() {
    change(null, status: RxStatus.success());
    super.onInit();
  }

  //* Text fields controllers
  TextEditingController fetchFirstName = TextEditingController(
    text: Get.find<DrawerXController>().userData.data!.userData!.firstName,
  );
  TextEditingController fetchLastName = TextEditingController(
    text: Get.find<DrawerXController>().userData.data!.userData!.lastName,
  );
  TextEditingController fetchEmail = TextEditingController(
    text: Get.find<DrawerXController>().userData.data!.userData!.email,
  );

  TextEditingController fetchPhoneNO = TextEditingController(
    text: Get.find<DrawerXController>().userData.data!.userData!.phoneNo,
  );
  TextEditingController fetchDOB = TextEditingController(
    text: Get.find<DrawerXController>().userData.data!.userData!.dob,
  );
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  //* Validation methods------------->>>>>>

  String? validFirstName(String? fname) {
    if (fname!.length < 3) {
      return "First Name atleast have 3 characters";
    }
  }

  String? validLastName(String? lname) {
    if (lname!.length < 3) {
      return "Last Name atleast have 3 characters";
    }
  }

  String? validPhoneNo(String? phone) {
    if (phone!.length < 10) {
      return "Enter a valid phone. no";
    }
  }

  String? validDOB(String? dob) {
    if (dob!.length < 11) {
      return "Enter DOB in as dd/mm/yyyy";
    }
  }

  String? validEmail(email) {
    if (email!.isEmpty || email == null) {
      return "Email Field cann't be Empty";
    } else if (!emailRex.hasMatch(email)) {
      return "Enter a valid Email";
    }
    return null;
  }

  String? validPassword(String? cpass) {
    if (oldPasswordController.text.isEmpty ||
        oldPasswordController.text.length < 6) {
      return "Password length can't be less then 6";
    } else {
      return null;
    }
  }

  String? validNewPassword(String? cpass) {
    if (passwordController.text != confirmPasswordController.text ||
        passwordController.text.isEmpty) {
      return "Password does not match";
    } else if (passwordController.text.length < 6) {
      return "Password length can't be less then 6";
    } else {
      return null;
    }
  }

  //* Other methods ----->>>>>>>>>>
  Future<void> resetPassword() async {
    if (resetPasswordFormKey.currentState!.validate()) {
      change(null, status: RxStatus.loading());
      ResetPasswordModel resetPasswordResponse =
          await MyAccountProvider().resetPassword(FormData({
        "old_password": oldPasswordController.text,
        "password": passwordController.text,
        "confirm_password": confirmPasswordController.text,
      }));
      change(null, status: RxStatus.success());

      if (resetPasswordResponse.status == 200) {
        Get.back();
        showGreenSnackbar(
            text: resetPasswordResponse.userMsg!, massage: "see you soon");
      } else {
        showRedSnackbar(
            "${resetPasswordResponse.userMsg!} | Status : ${resetPasswordResponse.status}",
            "may be try again");
      }
    }
  }

  //* Pic image from camera   ---->>>>>>>>>>>>
  void _openCamera(BuildContext context) async {
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
    );
    Get.back();
    if (pickedFile != null) {
      pickedImage = File(pickedFile.path);
    } else {
      pickedImage = null;
    }
    update();
  }

  //* Pic image from gallery >>>>>>>>>>>>
  void _openGallery(BuildContext context) async {
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
    );
    Get.back();
    if (pickedFile != null) {
      pickedImage = File(pickedFile.path);
    } else {
      pickedImage = null;
    }
    update();
  }

  //* Image picker Alert Dialog >>>>>>>>>
  Future<void> picImage(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            title: CustomText(
              text: "Pick an Image",
              size: 16,
              color: Colors.red.shade800,
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                    },
                    title: CustomText(
                      text: "Gallery",
                      color: Colors.red.shade800,
                    ),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.red.shade800,
                    ),
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                    },
                    title: CustomText(
                      text: "Camera",
                      color: Colors.red.shade800,
                    ),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.red.shade600,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

//* Edit user profile info
  Future<void> editProfileInfo() async {
    if (pickedImage == null) {
      showRedSnackbar("Image is required", "Please select your updated Image");
    } else {
      if (editProfieKey.currentState!.validate()) {
        Get.back();
        change(null, status: RxStatus.loading());
        final imageData = convertImageToBase64(imagePath: pickedImage!.path);

        FormData profileUpdateData = FormData({
          "first_name": fetchFirstName.text,
          "last_name": fetchLastName.text,
          "email": fetchEmail.text,
          "phone_no": fetchPhoneNO.text,
          "dob": fetchDOB.text,
          "profile_pic": imageData,
        });

        final profileUpdateResponse =
            await MyAccountProvider().updateProfile(profileUpdateData);
        change(null, status: RxStatus.success());
        if (profileUpdateResponse.status == 200) {
          await Get.find<DrawerXController>().fetchUserData();
          pickedImage = null;
          Get.back();
          showGreenSnackbar(
              text: profileUpdateResponse.userMsg!,
              massage: "We are glad to save you information");
        } else {
          showRedSnackbar(
            "${profileUpdateResponse.userMsg!}",
            "${profileUpdateResponse.status}",
          );
        }

        ;
      }
    }
  }

  String convertImageToBase64({required imagePath}) {
    final bytes = File(imagePath).readAsBytesSync();
    return "data:image/jpg;base64," + base64Encode(bytes);
  }

  RegExp emailRex = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
}
