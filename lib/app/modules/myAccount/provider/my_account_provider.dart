import 'dart:developer';

import 'package:get/get.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../model/reset_password_model.dart';
import '../model/update_profile_model.dart';

class MyAccountProvider extends GetConnect {
  Future<ResetPasswordModel> resetPassword(FormData data) async {
    try {
      final response = await post(
          "http://staging.php-dev.in:8844/trainingapp/api/users/change", data,
          headers: {
            "access_token": Get.find<AuthenticationController>().getToken!
          });
      log(response.body.toString());
      return resetPasswordModelFromJson(response.body);
    } catch (e) {
      return ResetPasswordModel(status: 502, userMsg: "Something went wrong");
    }
  }

  Future<UpdateProfileModel> updateProfile(FormData updateProfileData) async {
    try {
      final response = await post(
        "http://staging.php-dev.in:8844/trainingapp/api/users/update",
        updateProfileData,
        headers: {
          "access_token": Get.find<AuthenticationController>().getToken!,
        },
      );
      log(  response.body.toString());
      return updateProfileResponseModelFromJson(response.body);
    } catch (e) {
      log("Error while updating user profile $e");

      return UpdateProfileModel(status: 502, userMsg: "Something went wrong");
    }
  }
}
