import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../components/widgets/ProgressIndicator.dart';
import '../../../components/widgets/bgImage.dart';

import '../../../components/widgets/customButton.dart';
import '../../../components/widgets/customTextField.dart';
import '../controllers/my_account_controller.dart';

class ResetPasswordView extends GetView<MyAccountController> {
  const ResetPasswordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: controller.obx(
        (state) {
          return Stack(
            children: [
              BGimage(),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 40,
                ),
                child: Form(
                  key: controller.resetPasswordFormKey,
                  child: Column(
                    children: [
                      Spacer(),
                      Text(
                        "NeoSTORE",
                        style: Get.theme.textTheme.headline1,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      CustomTextField(
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.white,
                        ),
                        controller: controller.oldPasswordController,
                        labelText: "Current Password",
                        validator: controller.validPassword,
                      ),
                      CustomTextField(
                        validator: controller.validNewPassword,
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.white,
                        ),
                        controller: controller.passwordController,
                        labelText: "New Password",
                      ),
                      CustomTextField(
                        validator: controller.validNewPassword,
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.white,
                        ),
                        controller: controller.confirmPasswordController,
                        labelText: "Confirm Password",
                        errorColor: Colors.white,
                      ),
                      SizedBox(height: 30),
                      CustomButton(
                        navigation: controller.resetPassword,
                        text: "RESET PASSWORD",
                        textColor: Colors.red.shade800,
                        backgroundColor: Colors.white,
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
        onLoading: ProgressingIndicator(),
      ),
    );
  }
}
