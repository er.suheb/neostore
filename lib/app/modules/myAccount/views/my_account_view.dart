import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/widgets/ProgressIndicator.dart';
import '../../../components/widgets/accountDetailsField.dart';
import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/bgImage.dart';
import '../../../components/widgets/customButton.dart';
import '../../drawer/controllers/drawer_controller.dart';
import '../controllers/my_account_controller.dart';
import 'reset_password_view.dart';
import 'update_profile_view.dart';

class MyAccountView extends GetView<MyAccountController> {
  final userData = Get.find<DrawerXController>().userData.data!.userData;

  MyAccountView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(
        text: "My Account",
        size: 16,
        action: [Icon(Icons.search)],
        leading: InkWell(
          onTap: (() {
            Get.back();
          }),
          child: Icon(
            Icons.arrow_back,
          ),
        ),
      ),
      body: controller.obx(onLoading: ProgressingIndicator(), (state) {
        return Stack(
          children: [
            BGimage(),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Spacer(),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.black87,
                        backgroundImage: NetworkImage(
                          userData!.profilePic ??
                              "https://picsum.photos/200/300/?blur=2",
                        ),
                        radius: 70,
                      ),
                      SizedBox(height: 20),
                      AccountDetailsField(
                          icon: Icons.account_circle,
                          text: controller.fetchFirstName.text),
                      AccountDetailsField(
                          icon: Icons.account_circle,
                          text: controller.fetchLastName.text),
                      AccountDetailsField(
                          icon: Icons.email, text: controller.fetchEmail.text),
                      AccountDetailsField(
                          icon: Icons.phone,
                          text: controller.fetchPhoneNO.text),
                      AccountDetailsField(
                          icon: Icons.cake,
                          text: controller.fetchDOB.text.isEmpty
                              ? "Unknown"
                              : controller.fetchDOB.text),
                      SizedBox(height: 20),
                      CustomButton(
                        navigation: () => Get.to(() => EditProfileView()),
                        horizontalPadding: 0,
                        backgroundColor: Colors.white,
                        text: "EDIT PROFILE",
                        textColor: Colors.red.shade800,
                      ),
                    ],
                  ),
                ),
                Spacer(),
                CustomButton(
                  navigation: () => Get.to(() => ResetPasswordView()),
                  horizontalPadding: 0,
                  text: "RESET PASSWORD",
                  textColor: Colors.black87,
                  backgroundColor: Colors.white,
                )
              ],
            ),
          ],
        );
      }),
    );
  }
}
