import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../components/widgets/filledFileImage.dart';

import '../../../components/widgets/ProgressIndicator.dart';
import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/bgImage.dart';
import '../../../components/widgets/customButton.dart';
import '../../../components/widgets/customTextField.dart';
import '../../../components/widgets/empty_avtar.dart';
import '../../../components/widgets/filled_avtar.dart';
import '../../drawer/controllers/drawer_controller.dart';
import '../controllers/my_account_controller.dart';

class EditProfileView extends GetView<MyAccountController> {
  const EditProfileView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _keyboardVisible = MediaQuery.of(context).viewInsets.bottom != 0;
    return Scaffold(
      appBar: customAppBar(
        text: "Edit Profile",
        size: 16,
        action: [Icon(Icons.search)],
        leading: InkWell(
          onTap: (() {
            Get.back();
          }),
          child: Icon(
            Icons.arrow_back,
          ),
        ),
      ),
      body: controller.obx(
        (state) {
          return Stack(
            children: [
              BGimage(),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 40,
                ),
                child: Form(
                  key: controller.editProfieKey,
                  child: Column(
                    children: [
                      Spacer(),
                      !_keyboardVisible
                          ? Center(
                              child: GestureDetector(
                                onTap: () {
                                  Get.find<MyAccountController>()
                                      .picImage(context);
                                },
                                child: Container(
                                  child: GetBuilder<MyAccountController>(
                                    builder: ((controller) {
                                      if (controller.pickedImage == null) {
                                        return const EmptyImageAvatar();
                                      } else {
                                        return FilledFileImage(
                                            imageFile: controller.pickedImage!);
                                      }
                                    }),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox.shrink(),
                      SizedBox(
                        height: 20,
                      ),
                      CustomTextField(
                        // readOnly: true,
                        prefixIcon: Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                        controller: controller.fetchFirstName,
                        labelText: "First Name",
                      ),
                      CustomTextField(
                        // readOnly: true,
                        prefixIcon: Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                        controller: controller.fetchLastName,
                        labelText: "Last Name",
                      ),
                      CustomTextField(
                        validator: controller.validEmail,
                        prefixIcon: Icon(
                          Icons.mail,
                          color: Colors.white,
                        ),
                        controller: controller.emailController,
                        labelText: "Email",
                        errorColor: Colors.white,
                      ),
                      CustomTextField(
                        validator: controller.validPhoneNo,
                        prefixIcon: Icon(
                          Icons.phone,
                          color: Colors.white,
                        ),
                        controller: controller.phoneNoController,
                        labelText: "Phone No.",
                        errorColor: Colors.white,
                      ),
                      CustomTextField(
                        validator: controller.validDOB,
                        prefixIcon: Icon(
                          Icons.cake,
                          color: Colors.white,
                        ),
                        controller: controller.dobController,
                        labelText: "DOB",
                        errorColor: Colors.white,
                      ),
                      SizedBox(height: 30),
                      CustomButton(
                        navigation: controller.editProfileInfo,
                        text: "SUBMIT",
                        textColor: Colors.red.shade800,
                        backgroundColor: Colors.white,
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
        onLoading: ProgressingIndicator(),
      ),
    );
  }
}
