import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/controllers/authentication_controller.dart';
import '../../../components/widgets/snackbar.dart';
import '../model/current_user_model.dart';
import '../provider/login_provider.dart';

class LoginController extends GetxController {
  TextEditingController emainController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final loginKey = GlobalKey<FormState>();

  String? validPassword(String? value) {
    if (value.toString().isEmpty) {
      return "Please enter your password";
    }
  }

  String? validEmail(String? email) {
    if (email.toString().isEmpty) {
      return "Enter your email";
    }
  }

  void checkLoginButton() async {
    if (loginKey.currentState!.validate()) {
      print("button pressed");
      CurrentUserModel checkUser = CurrentUserModel(
        email: emainController.text,
        password: passwordController.text,
      );
      final resp = await LoginProvider().loginUser(checkUser);
      if (resp["status"] == 200) {
        await Get.find<AuthenticationController>().setToken(resp["token"]);
        // Get.find<AuthenticationController>().setToken(null);
        showGreenSnackbar(
            text: "Welcome!", massage: "You successfully logged in");
      } else {
        showRedSnackbar("Ooops!", "Server couldn't get you");
        // Get.snackbar(resp["status"].toString(), resp["msg"]);
      }
    }
  }
}
