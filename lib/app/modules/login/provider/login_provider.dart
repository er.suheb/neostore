import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';

import '../model/current_user_model.dart';

class LoginProvider extends GetConnect {
  Future<Map<String, dynamic>> loginUser(CurrentUserModel checkUser) async {
    FormData loginFormData = FormData(checkUser.toMap());

    try {
      log(checkUser.password.toString());
      final response = await post(
          "http://staging.php-dev.in:8844/trainingapp/api/users/login",
          loginFormData);

      log("response : ${response.body.toString()}");

      if (json.decode(response.body)["status"] == 200) {
        return {
          "msg": json.decode(response.body)["message"],
          "status": json.decode(response.body)["status"],
          "token": json.decode(response.body)["data"]["access_token"],
        };
      } else {
        return {
          "msg": json.decode(response.body)["message"],
          "status": json.decode(response.body)["status"],
        };
      }
    } catch (e) {
      log("Error while trying to login : $e");
      return {"msg": "something gone wrong"};
    }
  }
}
