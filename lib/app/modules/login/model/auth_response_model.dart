import 'dart:convert';

class AuthResponseModel {
  final int? id;
  final int? role_id;
  final String? first_name;
  final String? last_name;
  final String? email;
  final String? username;
  final String? profile_pic;
  final String? country_id;
  final String? gender;
  final int? phone_no;
  final int? dob;
  final bool? is_active;
  final String? created;
  final String? modified;
  final String? access_token;
  AuthResponseModel({
    this.id,
    this.role_id,
    this.first_name,
    this.last_name,
    this.email,
    this.username,
    this.profile_pic,
    this.country_id,
    this.gender,
    this.phone_no,
    this.dob,
    this.is_active,
    this.created,
    this.modified,
    this.access_token,
  });

  AuthResponseModel copyWith({
    int? id,
    int? role_id,
    String? first_name,
    String? last_name,
    String? email,
    String? username,
    String? profile_pic,
    String? country_id,
    String? gender,
    int? phone_no,
    int? dob,
    bool? is_active,
    String? created,
    String? modified,
    String? access_token,
  }) {
    return AuthResponseModel(
      id: id ?? this.id,
      role_id: role_id ?? this.role_id,
      first_name: first_name ?? this.first_name,
      last_name: last_name ?? this.last_name,
      email: email ?? this.email,
      username: username ?? this.username,
      profile_pic: profile_pic ?? this.profile_pic,
      country_id: country_id ?? this.country_id,
      gender: gender ?? this.gender,
      phone_no: phone_no ?? this.phone_no,
      dob: dob ?? this.dob,
      is_active: is_active ?? this.is_active,
      created: created ?? this.created,
      modified: modified ?? this.modified,
      access_token: access_token ?? this.access_token,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'role_id': role_id,
      'first_name': first_name,
      'last_name': last_name,
      'email': email,
      'username': username,
      'profile_pic': profile_pic,
      'country_id': country_id,
      'gender': gender,
      'phone_no': phone_no,
      'dob': dob,
      'is_active': is_active,
      'created': created,
      'modified': modified,
      'access_token': access_token,
    };
  }

  factory AuthResponseModel.fromMap(Map<String, dynamic> map) {
    return AuthResponseModel(
      id: map['id']?.toInt() ?? 0,
      role_id: map['role_id']?.toInt() ?? 0,
      first_name: map['first_name'] ?? '',
      last_name: map['last_name'] ?? '',
      email: map['email'] ?? '',
      username: map['username'] ?? '',
      profile_pic: map['profile_pic'] ?? '',
      country_id: map['country_id'] ?? '',
      gender: map['gender'] ?? '',
      phone_no: map['phone_no']?.toInt() ?? 0,
      dob: map['dob']?.toInt() ?? 0,
      is_active: map['is_active'] ?? false,
      created: map['created'] ?? '',
      modified: map['modified'] ?? '',
      access_token: map['access_token'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory AuthResponseModel.fromJson(String source) =>
      AuthResponseModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ApiResponse(id: $id, role_id: $role_id, first_name: $first_name, last_name: $last_name, email: $email, username: $username, profile_pic: $profile_pic, country_id: $country_id, gender: $gender, phone_no: $phone_no, dob: $dob, is_active: $is_active, created: $created, modified: $modified, access_token: $access_token)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AuthResponseModel &&
        other.id == id &&
        other.role_id == role_id &&
        other.first_name == first_name &&
        other.last_name == last_name &&
        other.email == email &&
        other.username == username &&
        other.profile_pic == profile_pic &&
        other.country_id == country_id &&
        other.gender == gender &&
        other.phone_no == phone_no &&
        other.dob == dob &&
        other.is_active == is_active &&
        other.created == created &&
        other.modified == modified &&
        other.access_token == access_token;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        role_id.hashCode ^
        first_name.hashCode ^
        last_name.hashCode ^
        email.hashCode ^
        username.hashCode ^
        profile_pic.hashCode ^
        country_id.hashCode ^
        gender.hashCode ^
        phone_no.hashCode ^
        dob.hashCode ^
        is_active.hashCode ^
        created.hashCode ^
        modified.hashCode ^
        access_token.hashCode;
  }
}
