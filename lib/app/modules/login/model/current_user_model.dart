

class CurrentUserModel {
  final String email;
  final String password;

  CurrentUserModel({
    required this.email,
    required this.password,
  });

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'password': password,
    };
  }

  @override
  String toString() => 'CurrentUser(email: $email, password: $password)';
}
