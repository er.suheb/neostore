import 'dart:convert';

class ApiResponse {
  final int? status;
  final ResponseData? data;
  final String? message;
  final String? userMsg;
  ApiResponse({
    this.status,
    this.data,
    this.message,
    this.userMsg,
  });
}

class ResponseData {
  final int? id;
  final int? roleId;
  final String? firstName;
  final String? lastName;
  final String? email;
  final String? username;
  final String? profilePic;
  final dynamic countryId;
  final String? gender;
  final String? phoneNo;
  final String? dob;
  final bool? isActive;
  final String? created;
  final String? modified;
  final String? accessToken;
  ResponseData({
    this.id,
    this.roleId,
    this.firstName,
    this.lastName,
    this.email,
    this.username,
    this.profilePic,
    required this.countryId,
    this.gender,
    this.phoneNo,
    this.dob,
    this.isActive,
    this.created,
    this.modified,
    this.accessToken,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'roleId': roleId,
      'firstName': firstName,
      'lastName': lastName,
      'email': email,
      'username': username,
      'profilePic': profilePic,
      'countryId': countryId,
      'gender': gender,
      'phoneNo': phoneNo,
      'dob': dob,
      'isActive': isActive,
      'created': created,
      'modified': modified,
      'accessToken': accessToken,
    };
  }

  factory ResponseData.fromMap(Map<String, dynamic> map) {
    return ResponseData(
      id: map['id']?.toInt(),
      roleId: map['roleId']?.toInt(),
      firstName: map['firstName'],
      lastName: map['lastName'],
      email: map['email'],
      username: map['username'],
      profilePic: map['profilePic'],
      countryId: map['countryId'] ?? null,
      gender: map['gender'],
      phoneNo: map['phoneNo'],
      dob: map['dob'],
      isActive: map['isActive'],
      created: map['created'],
      modified: map['modified'],
      accessToken: map['accessToken'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ResponseData.fromJson(String source) =>
      ResponseData.fromMap(json.decode(source));
}
