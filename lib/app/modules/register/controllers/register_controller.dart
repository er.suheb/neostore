import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/enums.dart';
import '../../../components/widgets/snackbar.dart';
import '../../../routes/app_pages.dart';
import '../models/register_user_model.dart';
import '../provider/user_provider.dart';

class RegisterController extends GetxController {
  //? Observable varialbes -------------->>>>>>>>>>>>
  var genderType = Gender.male.obs;
  var check = false.obs;

  final registrationFormKey = GlobalKey<FormState>();

  //? TextEditing Controllers ------------- >>>>>>>>>>>
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController emainController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  void toggleCheck(bool? val) {
    check.value = val!;
    print(val);
  }

  String genderCheck() {
    if (genderType.value == Gender.male) {
      return "Male";
    } else {
      return "Female";
    }
  }

  String? validpassword(String? value) {
    if (value!.length < 6) {
      return "Password must contain Special, Captial, Small and Numeric Characters";
    }
  }

  String? validFirstName(String? fname) {
    if (fname!.length < 3) {
      return "First Name atleast have 3 characters";
    }
  }

  String? validConfirmPassword(String? cpass) {
    if (confirmPasswordController.text != passwordController.text ||
        confirmPasswordController.text.isEmpty) {
      return "Password does not match";
    }
  }

  String? validLastName(String? lname) {
    if (lname!.length < 3) {
      return "Last Name atleast have 3 characters";
    }
  }

  String? validPhoneNo(String? phone) {
    if (phone!.length < 10) {
      return "Enter a valid phone. no";
    }
  }

  

  String? validEmail(email) {
    if (email!.isEmpty || email == null) {
      return "Email Field cann't be Empty";
    } else if (!emailRex.hasMatch(email)) {
      return "Enter a valid Email";
    }
    return null;
  }

  checkRegisterButton() async {
    if (registrationFormKey.currentState!.validate()) {
      log("register button pressed");
      RegisterUserModel userModel = RegisterUserModel(
        first_name: firstNameController.text,
        last_name: lastNameController.text,
        email: emainController.text,
        password: passwordController.text,
        confirm_password: confirmPasswordController.text,
        gender: genderType.value == Gender.male ? "M" : "F",
        phone_no: int.parse(phoneNoController.text),
      );
      log(userModel.toString());
      final msg = await UserProvider().registerUser(userModel);

      print(msg['msg']);
      print(msg["status"]);
      Get.offAllNamed(Routes.LOGIN);
      if (msg["status"] == 200) {
        showGreenSnackbar(
            text: "Account created", massage: "Login to enter at NeoSTORE");
      } else {
        showRedSnackbar("Ooops!", "Server couldn't get you");
        // Get.snackbar(resp["status"].toString(), resp["msg"]);
      }
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  RegExp emailRex = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
}
