import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';

import '../models/register_user_model.dart';

class UserProvider extends GetConnect {
  Future<Map<String, dynamic>> registerUser(RegisterUserModel newUser) async {
    FormData userData = FormData(newUser.toMap());

    try {
      final response = await post(
        "http://staging.php-dev.in:8844/trainingapp/api/users/register",
        userData,
      );
      log(response.body);
      return {
        "msg": json.decode(response.body)["message"],
        "status": json.decode(response.body)["status"],

        
      };
    } catch (e) {
      log("Api fetch error : $e");
      return {
        'msg': "Api fetch error $e",
        "status": 400,
      };

    }
    
  }
}
