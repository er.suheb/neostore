

class RegisterUserModel {
  final String first_name;
  final String last_name;
  final String email;
  final String password;
  final String confirm_password;
  final String gender;
  final int phone_no;
  RegisterUserModel({
    required this.first_name,
    required this.last_name,
    required this.email,
    required this.password,
    required this.confirm_password,
    required this.gender,
    required this.phone_no,
  });

  Map<String, dynamic> toMap() {
    return {
      'first_name': first_name,
      'last_name': last_name,
      'email': email,
      'password': password,
      'confirm_password': confirm_password,
      'gender': gender,
      'phone_no': phone_no,
    };
  }

  @override
  String toString() {
    return 'RegisterUserModel(first_name: $first_name, last_name: $last_name, email: $email, password: $password, confirm_password: $confirm_password, gender: $gender, phone_no: $phone_no)';
  }
}
