import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:get/get_connect/connect.dart';

import '../controllers/forgot_password_controller.dart';

class RecoveryPassword extends GetConnect {
  Future<Map<String, dynamic>> recoverpassword() async {
    FormData recoverpasswordFormData = FormData(
        {"email": Get.find<ForgotPasswordController>().emailctrl.text});
    final response = await post(
        "http://staging.php-dev.in:8844/trainingapp/api/users/forgot",
        recoverpasswordFormData);

    log(response.body.toString());
    return {"email": jsonDecode(response.body)["email"]};
  }
}
