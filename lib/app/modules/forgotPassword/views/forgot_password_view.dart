import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../components/widgets/bgImage.dart';
import '../../../components/widgets/customButton.dart';
import '../../../components/widgets/customTextField.dart';
import '../../../routes/app_pages.dart';
import '../controllers/forgot_password_controller.dart';

class ForgotPasswordView extends GetView<ForgotPasswordController> {
  const ForgotPasswordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        const BGimage(),
        Form(
          key: controller.forgotFormKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: Text(
                      "Forgot Password",
                      style: Theme.of(context).textTheme.headline2,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Text(
                  "Enter your registered email so we can send you an OTP to verify",
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: CustomTextField(
                  controller: controller.emailctrl,
                  validator: (email) {
                    controller.validEmail(email);
                  },
                  labelText: "Enter your email",
                ),
              ),
              SizedBox(
                height: 12,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: InkWell(
                  onTap: (() {
                    Get.offAllNamed(Routes.LOGIN);
                  }),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "Password Remembered ?",
                        style: Theme.of(context).textTheme.headline3,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              CustomButton(
                  navigation: () {
                    controller.checkForgotPassword();
                  },
                  text: "Send",
                  textColor: Colors.red,
                  backgroundColor: Colors.white)
            ],
          ),
        ),
      ]),
    );
  }
}
