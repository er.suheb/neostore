import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../provider/recovery_provider.dart';

class ForgotPasswordController extends GetxController {
  final forgotFormKey = GlobalKey<FormState>();
  TextEditingController emailctrl = TextEditingController();
  //TODO: Implement ForgotPasswordController

  String? validEmail(email) {
    if (email!.isEmpty || email == null) {
      return "Email Field cann't be Empty";
    } else if (!emailRex.hasMatch(email)) {
      return "Enter a valid Email";
    }
    return null;
  }

  void checkForgotPassword() {
    if (forgotFormKey.currentState!.validate()) {
      RecoveryPassword().recoverpassword();
      log("forgot password button pressed");
    }
  }

  RegExp emailRex = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
}
