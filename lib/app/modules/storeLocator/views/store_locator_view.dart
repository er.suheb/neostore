import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../components/widgets/appbar.dart';
import '../../../components/widgets/customText.dart';
import '../controllers/store_locator_controller.dart';

class StoreLocatorView extends GetView<StoreLocatorController> {
  const StoreLocatorView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: customAppBar(
          text: "Store Locator",
          size: 16,
          action: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: Icon(Icons.search),
            )
          ],
          leading: InkWell(
            onTap: (() {
              Get.back();
            }),
            child: const Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 300,
                child: const GoogleMap(
                  initialCameraPosition: CameraPosition(
                    target: LatLng(28.535517, 77.391029),
                    zoom: 15,
                  ),
                ),
              ),
              ListTile(
                title: CustomText(
                  text: "SKYLAND STORE",
                  color: Colors.black87,
                ),
                subtitle: CustomText(
                  text: "6335 Edgewood Road ReisterTown MD 21136",
                  color: Colors.black54,
                  fontName: "GothamLight",
                ),
                leading: Icon(Icons.pin_drop),
              ),
              Divider(),
              ListTile(
                title: CustomText(
                  text: "WOODMOUNT STORE",
                  color: Colors.black87,
                ),
                subtitle: CustomText(
                  text: "9437 Pin Oak Drive South Plainfield, NJ 07080",
                  color: Colors.black54,
                  fontName: "GothamLight",
                ),
                leading: Icon(Icons.pin_drop),
              ),
              Divider(),
              ListTile(
                title: CustomText(
                  text: "NATUFUR STORE",
                  color: Colors.black87,
                ),
                subtitle: CustomText(
                  text: "3789 Pennsylvania Avenue Brandon, FL 33510",
                  color: Colors.black54,
                  fontName: "GothamLight",
                ),
                leading: Icon(Icons.pin_drop),
              ),
              Divider(),
              ListTile(
                title: CustomText(
                  text: "LAVANDAR STORE",
                  color: Colors.black87,
                ),
                subtitle: CustomText(
                  text: "9311 Garfield Avanue Hamburg, NY 1407",
                  color: Colors.black54,
                  fontName: "GothamLight",
                ),
                leading: Icon(Icons.pin_drop),
              ),
              Divider(),
              ListTile(
                title: CustomText(
                  text: "FURNIMATT STORE",
                  color: Colors.black87,
                ),
                subtitle: CustomText(
                  text: "7346 Hanover Court Arlington, MA 02474",
                  color: Colors.black54,
                  fontName: "GothamLight",
                ),
                leading: Icon(Icons.pin_drop),
              ),
              Divider(),
            ],
          ),
        ));
  }
}
