import 'package:get/get.dart';

import '../controllers/store_locator_controller.dart';

class StoreLocatorBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StoreLocatorController>(
      () => StoreLocatorController(),
    );
  }
}
