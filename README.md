# neostore

You can download NeoSTORE APK file from [here](https://1drv.ms/u/s!Ao5qgWE_9rYRlQVmiTQrVcA9DYnV?e=fKI1mF)
![LogInScreen](https://user-images.githubusercontent.com/56574011/174266001-c700f2c0-cd83-4dd8-8232-65aec7403581.png)
![ForgotScreen](/uploads/33eb22f07fa46691b1753414e7b8104b/image.png)
![RegisterScreen](/uploads/344224167b98ea7f080b9b5b55766424/image.png)
![HomeScreen](/uploads/81e3bc42bc0840f8339aeb8709500378/image.png)
![DrawerScreen](/uploads/c90fbca8bc347f794bce6b2c0bb2c74a/image.png)

<!-- ![DrawerScreen](/uploads/29db2cb1a09073319ac31ceb1d5307e7/image.png) -->

![Product_ListScreen](/uploads/d658d106e9f2ce5253397f9f1032a8c7/image.png)
![Prodcut_DetailedScreen](/uploads/f89583d5a8d7df2e14a3bd97a476a198/image.png)

![Rating_Screen](/uploads/57d5eb0075c3213b67b54b8951b51afe/Screenshot_from_2022-07-05_13-18-51.png)
![checkOut_Product_Screen](/uploads/02f51b3d12cb73aa5866ae1bed66b80e/image.png)
![MyCart_Screen](/uploads/770c4a14ce5d1eed0cab93d1c44c6593/image.png)
![Address_Screen](/uploads/1a6759b82daa6689ca927e32eb56a5da/image.png)
![MyOrder_Screen](/uploads/3004e96bbecb11ad408b2690f494940d/image.png)
![OrderDetails_Screen](/uploads/4dc2b350d4ce2236c9d7dc992a005026/image.png)
![Update_Profile_Screen](/uploads/0329c6216c9a5249143c333a60308bbf/image.png)
![Store_Locator](/uploads/2ddcd117c357e6fd6dc89c76dd702856/image.png)
